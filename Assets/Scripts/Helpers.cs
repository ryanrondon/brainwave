﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class Helpers {

	public static float DistanceBetweenObjects (GameObject src, GameObject tgt) {
		return Mathf.Sqrt (Mathf.Pow(tgt.transform.position.y - src.transform.position.y,2) + Mathf.Pow(tgt.transform.position.x - src.transform.position.x,2));
	}

	public static float AngleBetweenObjects (GameObject src, GameObject tgt) {
		return Mathf.Atan2(tgt.transform.position.y - src.transform.position.y, tgt.transform.position.x - src.transform.position.x);
	}

	public static float AngleBetweenPoints (Vector3 src, Vector3 tgt) {
		return Mathf.Atan2(tgt.y - src.y, tgt.x - src.x);
	}

	public static Vector2 CameraRelativePos (GameObject src) {
		var cameraTransform = Camera.main.transform;
		return cameraTransform.InverseTransformDirection (src.transform.position - cameraTransform.position);
	}

	public static Vector3 RandomPointInScreen () {
		var cameraTransform = Camera.main.transform;
		return cameraTransform.position + new Vector3 (Random.Range (-5, 5), Random.Range (-3, 3), 10);
	}

	public static void PlayAudio (AudioClip src, float vol) {
		if (Camera.main) {
			var player = Camera.main.GetComponent <AudioSource> ();
			player.PlayOneShot (src, vol);
		}
	}
}