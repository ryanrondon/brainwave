﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyManager : MonoBehaviour {
	List <GameObject> EnemyList;
	public GameObject Player;
	public int StartingEnemies;
	int _enemyWaveCount;
	int _waveNumber = 0;
	Object _enemy1;
	Object _enemy2;
	Object _enemy3;
	Object _fx;
	bool _isSpawning;

	AudioClip _sfxZoom;
	AudioClip _sfxGameOver;
	AudioClip _sfxSpawn;

	// Use this for initialization
	void Start () {
		EnemyList = new List<GameObject> ();
		_enemy1 = Resources.Load ("Enemy1");
		_enemy2 = Resources.Load ("Enemy2");
		_enemy3 = Resources.Load ("Enemy3");
		_fx = Resources.Load ("Spawn");

		_sfxZoom = Resources.Load<AudioClip> ("Audio/zoom");
		_sfxSpawn = Resources.Load<AudioClip> ("Audio/ding");
		_sfxGameOver = Resources.Load<AudioClip> ("Audio/game_over");

		StartGame ();
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < EnemyList.Count;) {
			if (EnemyList[i] == null) {
				EnemyList.RemoveAt (i);
				EnemyList.TrimExcess ();
			}
			else {
				i++;
			}
		}
		if (EnemyList.Count == 0 && !_isSpawning) {
			StartingEnemies += 2;
			StartCoroutine (spawnEnemies (StartingEnemies));
		}
	}

	public void StartGame () {
		StartCoroutine (spawnEnemies (StartingEnemies));
	}

	IEnumerator spawnEnemies ( int max ) {
		Helpers.PlayAudio (_sfxZoom, 1);

		_isSpawning = true;

		_waveNumber++;
		var waveCorner = GameObject.Find ("WaveCorner");
		var waveText = GameObject.Find ("WaveIndicator");
		waveText.GetComponent <Text>().text = "WAVE " + _waveNumber;
		waveCorner.GetComponent <Text>().text = "WAVE " + _waveNumber;
		waveText.GetComponent <RectTransform> ().localScale = new Vector2 (8, 8);
		LeanTween.scale (waveText.GetComponent <RectTransform> (), new Vector3 (1, 1, 1), 0.33f);

		yield return new WaitForSeconds (0.66f);

		LeanTween.scale (waveText.GetComponent <RectTransform> (), new Vector3 (0, 0, 0), 0.33f);

		yield return new WaitForSeconds (0.33f);

		float vol = 1.0f;
		for (int i = 0; i < max; i++) {
			Helpers.PlayAudio (_sfxSpawn, vol);
			vol = vol * 0.8f;

			var randPos = Helpers.RandomPointInScreen ();
			Object randEnemy;

			var perc = Random.Range (0.0f, 1.0f);
			if (perc <= 0.70f) {
				randEnemy = _enemy1;
			}
			else if (perc <= 0.85f) {
				randEnemy = _enemy2;
			}
			else {
				randEnemy = _enemy3;
			}

			var enemy = (GameObject) Instantiate (randEnemy, randPos, new Quaternion (), gameObject.transform);
			enemy.transform.position = randPos;
			enemy.GetComponent <EnemySwarmVelocity> ().Target = Player;
			if (enemy.GetComponent <EnemyShoot> ()) {
				enemy.GetComponent <EnemyShoot> ().Level += Mathf.FloorToInt (_waveNumber / 5.0f);
			}
			EnemyList.Add (enemy);

			var exp = (GameObject) Instantiate(_fx, randPos, new Quaternion (), gameObject.transform.parent);
			Destroy (exp, 2);

			yield return new WaitForSeconds (0.3f);
		}

		_isSpawning = false;
	}

	public void GameOver () {
		StartCoroutine (gameOverSeq ());
	}

	IEnumerator gameOverSeq () {
		Helpers.PlayAudio (_sfxGameOver, 1);

		_waveNumber++;
		var waveText = GameObject.Find ("WaveIndicator");
		waveText.GetComponent <Text> ().text = "DESYNCHRONIZED!";
		waveText.GetComponent <RectTransform> ().localScale = new Vector2 (8, 8);
		LeanTween.scale (waveText.GetComponent <RectTransform> (), new Vector3 (1, 1, 1), 0.33f);

		yield return new WaitForSeconds (1.33f);

		LeanTween.scale (waveText.GetComponent <RectTransform> (), new Vector3 (0, 0, 0), 0.33f);

		yield return new WaitForSeconds (0.33f);

		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}
}
