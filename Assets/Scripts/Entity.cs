﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour {
	public int Life = 2;
	bool _isDead = false;
	public void onDamage (int dmg) {
		Life -= dmg;
		if ( Life <= 0 && !_isDead) {
			Destroy (gameObject);
			_isDead = true;
			if (GameObject.Find ("Player") != null) {
				GameObject.Find ("Player").GetComponent <PlayerShoot> ().OnKill (gameObject);
			}
		}
	}
}
