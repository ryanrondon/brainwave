﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq.Expressions;
using System.CodeDom.Compiler;

public class Bullet : MonoBehaviour {
	public Vector2 FinalVelocity = new Vector2 ();

	Rigidbody2D _rigidBody;
	Vector2 _pos;
	public int Life = 1;

	public float Timeout = 2;
	static Object _hitExp = null;
	static AudioClip _hitSfx;

	public GameObject HomingTarget = null;
	public float HomingSpeed = 0.045f;
	public float HomingRate = 5;
	float _angleToTarget;
	float _lerpAngle = -999;
	bool _didHoming;


	// Use this for initialization
	void Start () {
		if (_hitExp == null) {
			_hitExp = Resources.Load ("BulletHit");
		}
		if (_hitSfx == null) {
			_hitSfx = Resources.Load <AudioClip> ("Audio/hit");
		}
		_rigidBody = gameObject.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		Timeout -= Time.deltaTime;
		if (Timeout <= 0 || Life <= 0) {
			Destroy (gameObject);
		}
	}

	void homeOnTarget () {
		if (HomingTarget != null) {
			//if ( _reAngleTimeout <=0 ) {
				float angle = Helpers.AngleBetweenObjects (gameObject, HomingTarget);
				_angleToTarget = angle;
				if (_lerpAngle == -999) {
					_lerpAngle = _angleToTarget + Mathf.Deg2Rad *Random.Range (-120, 120);
					_didHoming = true;
				}
			//	_reAngleTimeout = (Mathf.Rad2Deg * angle) / (HomingRate * 50);
			//}
			//else {
			//	_reAngleTimeout -= Time.fixedDeltaTime;
			//}

			_lerpAngle = Mathf.MoveTowardsAngle (_lerpAngle, _angleToTarget, HomingRate * Time.fixedDeltaTime);
			FinalVelocity.x = Mathf.Cos (_lerpAngle) * HomingSpeed;
			FinalVelocity.y = Mathf.Sin (_lerpAngle) * HomingSpeed;

			gameObject.transform.eulerAngles = new Vector3(0, 0, _lerpAngle * Mathf.Rad2Deg);
		}

		if (_didHoming && HomingTarget == null && gameObject.tag == "Player") {
			var allNeighbors = Physics2D.OverlapCircleAll (gameObject.transform.position, 3, 1 << LayerMask.NameToLayer ("Enemies"));
			foreach (var c in allNeighbors) {
				if (c.gameObject.GetComponent <EnemySwarmVelocity> () ) {
					HomingTarget = c.gameObject;
				}
			}
		}
	}

	void OnDestroy () {
		var exp = (GameObject) Instantiate(_hitExp, gameObject.transform.position, new Quaternion (), gameObject.transform.parent);

		var col = (gameObject.tag == "Player") ? Color.cyan : new Color (1,0.5f,0);
		foreach (var ps in exp.GetComponentsInChildren <ParticleSystem> ()) {
			var module = ps.main;
			module.startColor = new ParticleSystem.MinMaxGradient (col);
		}

		Destroy (exp, 2);

		if (Timeout > 0) {
			Helpers.PlayAudio (_hitSfx, 0.33f);
		}
	}

	void FixedUpdate () {
		if (!gameObject.GetComponent <Entity> ()) {
			homeOnTarget ();
			_pos = _rigidBody.position;
			_pos = _pos + FinalVelocity;
			_rigidBody.MovePosition (_pos);
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.GetComponent <Bullet> () && 
			other.gameObject.tag != gameObject.tag) {
			Life -= 1;
			other.gameObject.GetComponent <Bullet> ().Life -= 1;
			if (other.GetComponent <Entity>()) {
				other.gameObject.GetComponent <Entity> ().onDamage (1);
				other.gameObject.SendMessage ("OnHit", null, SendMessageOptions.DontRequireReceiver);
			}
			else if (GetComponent <Entity>()) {
				gameObject.GetComponent <Entity> ().onDamage (1);
				gameObject.SendMessage ("OnHit", null, SendMessageOptions.DontRequireReceiver);
			}
			Camera.main.GetComponent <CameraTarget> ().ShakeCamera (0.01f, 0.01f, 0.1f);
			return;
		}

		if (other.gameObject.GetComponent <Entity> () && other.gameObject.tag != gameObject.tag) {
			other.gameObject.GetComponent <Entity> ().onDamage (1);
			other.gameObject.SendMessage ("OnHit", null, SendMessageOptions.DontRequireReceiver);
			Destroy (gameObject);
			Camera.main.GetComponent <CameraTarget> ().ShakeCamera (0.03f, 0.03f, 0.2f);
		}
	}
}
