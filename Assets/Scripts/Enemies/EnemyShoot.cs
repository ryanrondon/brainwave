﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour {
	public int Level = 1;
	public int ShootIndex = 1;
	float _cooldown;
	Object _bullet;
	Object _missile;
	Object _flash;
	bool _didFlash = false;

	static AudioClip _sfxFire;
	static AudioClip _sfxMissile;

	// Use this for initialization
	void Start () {
		_cooldown = Random.Range (1.0f, 3.0f);
		_bullet = Resources.Load ("Bullet");
		_missile = Resources.Load ("Missile");
		_flash = Resources.Load ("Flash");

		if (_sfxFire == null) {
			_sfxFire = Resources.Load <AudioClip> ("Audio/enemy_fire");
		}

		if (_sfxMissile == null) {
			_sfxMissile = Resources.Load <AudioClip> ("Audio/enemy_mis");
		}
	}
	
	// Update is called once per frame
	void Update () {
		_cooldown -= Time.deltaTime;
		if (_cooldown <= 0.4f && !_didFlash) {
			_didFlash = true;
			var fx = (GameObject)Instantiate (_flash);
			fx.transform.SetParent (gameObject.transform);
			fx.transform.localPosition = Vector3.zero;
			fx.transform.localScale = new Vector3 (10.0f, 10.0f, 10.0f);
			Destroy (fx, 2);
		}

		if (_cooldown <= 0) {
			if (ShootIndex == 1) {
				shoot ();
			}
			else if (ShootIndex == 2) {
				missiles ();
			}
		}
	}

	void shoot () {
		if (gameObject.GetComponent <EnemySwarmVelocity> ().Target == null) {
			return;
		}

		if (Helpers.DistanceBetweenObjects (gameObject, gameObject.GetComponent <EnemySwarmVelocity> ().Target) < 0.5f) {
			return;
		}

		_didFlash = false;
		var bullet = (GameObject) Instantiate(_bullet, gameObject.transform.position, new Quaternion (), gameObject.transform.parent);

		float angle = gameObject.transform.rotation.eulerAngles.z * Mathf.Deg2Rad; //Helpers.AngleBetweenObjects (gameObject, Target);
		Debug.Log (angle * Mathf.Rad2Deg);
		Vector2 bVel;
		bVel.x = Mathf.Cos (angle) * (0.03f + (0.02f * Level));
		bVel.y = Mathf.Sin (angle) * (0.03f + (0.02f * Level));

		bullet.GetComponent<Bullet> ().FinalVelocity = bVel;

		_cooldown = 2.5f;

		Helpers.PlayAudio (_sfxFire, 0.5f);
	}

	void missiles () {
		if (gameObject.GetComponent <EnemySwarmVelocity> ().Target == null) {
			return;
		}

		if (Helpers.DistanceBetweenObjects (gameObject, gameObject.GetComponent <EnemySwarmVelocity> ().Target) < 0.5f) {
			return;
		}

		_didFlash = false;
		var missile = (GameObject) Instantiate(_missile, gameObject.transform.position, new Quaternion (), gameObject.transform.parent);
		missile.GetComponent <Bullet> ().HomingTarget = gameObject.GetComponent <EnemySwarmVelocity> ().Target;

		_cooldown = 4.5f - (0.75f * Level);
		_cooldown = Mathf.Max (1.0f, _cooldown);
		Helpers.PlayAudio (_sfxMissile, 0.5f);
	}
}
