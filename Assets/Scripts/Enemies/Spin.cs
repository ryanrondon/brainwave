﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour {
	Vector3 _inc;

	// Use this for initialization
	void Start () {
		_inc = new Vector3 (0, 0, 5);
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.eulerAngles = gameObject.transform.eulerAngles + _inc;
	}
}
