﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySwarmVelocity : MonoBehaviour {
	public float Speed = 0.02f;
	public float TurnRate = 3;
	public Vector2 FinalVelocity;
	Rigidbody2D _rigidBody;
	Vector2 _pos;

	float _angleToTarget;
	float _lerpAngle;

	float _alignRad = 1.5f;
	float _cohRad = 1.5f;
	float _sepRad = 0.5f;

	float _alignFactor = 100;
	float _cohFactor = 300;
	float _sepFactor = 50;

	float _warpTimeout = 0;
	float _reAngleTimeout;

	public GameObject Target;
	static Object _explosion;
	static AudioClip _sfxExplode;

	// Use this for initialization
	void Start () {
		FinalVelocity = new Vector2 (0.02f, 0);
		_rigidBody = gameObject.GetComponent<Rigidbody2D> ();

		if (_explosion == null) {
			_explosion = Resources.Load ("Explosion");
		}
		if (_sfxExplode == null) {
			_sfxExplode = Resources.Load <AudioClip> ("audio/enemy_die");
		}
	}

	// Update is called once per frame
	void Update () {
		if (_warpTimeout >= 0) {
			_warpTimeout -= Time.deltaTime;
		}
	}

	void setAngleToTarget () {
		if (Target == null) {
			return; 
		}

		if ( _reAngleTimeout <= 0) {
			float angle = Helpers.AngleBetweenObjects (gameObject, Target);

			//if (Helpers.DistanceBetweenObjects (gameObject, Target) < 2) {
			//	if (Mathf.Cos(angle) < 0) {
			//		angle += Mathf.Deg2Rad * 30;
			//	}
			//	else {
			//		angle -= Mathf.Deg2Rad * 30;
			//	}
			//}
			_angleToTarget = angle;
			_reAngleTimeout = (Mathf.Rad2Deg * angle) / (TurnRate * 30.0f);
		}
		else {
			_reAngleTimeout -= Time.fixedDeltaTime;
		}

		_lerpAngle = Mathf.MoveTowardsAngle (_lerpAngle, _angleToTarget, TurnRate * Time.fixedDeltaTime);

		FinalVelocity.x = Mathf.Cos (_lerpAngle) * Speed;
		FinalVelocity.y = Mathf.Sin (_lerpAngle) * Speed;
	}

	Vector2 computeAlignment () {
		Vector2 velocity = new Vector2 ();
		int neighborCount = 0;

		var allNeighbors = Physics2D.OverlapCircleAll (gameObject.transform.position, _alignRad, 1 << LayerMask.NameToLayer ("Enemies"));
		foreach (var c in allNeighbors) {
			if (c.gameObject != gameObject) {
				velocity += gameObject.GetComponent<EnemySwarmVelocity> ().FinalVelocity;
				neighborCount++;
			}
		}
		if (neighborCount > 0) {
			velocity.x /= neighborCount;
			velocity.y /= neighborCount;
			velocity.Normalize ();
			velocity /= _alignFactor;
		}

		//Debug.Log ("Alignment " + gameObject.name + " " + velocity);

		return velocity;
	}

	Vector2 computeCohesion () {
		Vector2 velocity = new Vector2 ();
		int neighborCount = 0;

		var allNeighbors = Physics2D.OverlapCircleAll (gameObject.transform.position, _cohRad, 1 << LayerMask.NameToLayer ("Enemies"));
		foreach (var c in allNeighbors) {
			if (c.gameObject != gameObject) {
				velocity += (Vector2) Helpers.CameraRelativePos (c.gameObject);
				neighborCount++;
				if (neighborCount >= 2) {
					break;
				}
			}
		}

		if (neighborCount > 0) {
			velocity.x /= neighborCount;
			velocity.y /= neighborCount;
			velocity = new Vector2 (velocity.x - Helpers.CameraRelativePos (gameObject).x, velocity.y - Helpers.CameraRelativePos (gameObject).y);
			velocity.Normalize ();
			velocity /= _cohFactor;
		}

		//Debug.Log ("Cohesion " + gameObject.name + " " + velocity);

		return velocity;
	}

	Vector2 computeSeparation () {
		Vector2 velocity = new Vector2 ();
		int neighborCount = 0;

		var allNeighbors = Physics2D.OverlapCircleAll (gameObject.transform.position, _sepRad, 1 << LayerMask.NameToLayer ("Enemies"));
		foreach (var c in allNeighbors) {
			if (c.gameObject != gameObject) {
				velocity += (Vector2) Helpers.CameraRelativePos (c.gameObject) - (Vector2) Helpers.CameraRelativePos (gameObject);
				neighborCount++;
				if (neighborCount >= 2) {
					break;
				}
			}
		}

		if (neighborCount > 0) {
			velocity.x /= neighborCount;
			velocity.y /= neighborCount;
			velocity = new Vector2 (velocity.x - Helpers.CameraRelativePos (gameObject).x, velocity.y - Helpers.CameraRelativePos (gameObject).y);
			velocity *= -1;
			velocity.Normalize ();
			velocity /= _sepFactor;
		}

		//Debug.Log ("Separation " + gameObject.name + " " + velocity);

		return velocity;
	}

	void FixedUpdate () {
		setAngleToTarget ();
		FinalVelocity += computeAlignment () + computeCohesion () + computeSeparation ();

		_pos = _rigidBody.position;
		_pos = _pos + FinalVelocity;


//		if (_warpTimeout <= 0) {
//			if (_pos.x <= -3) {
//				_pos.x = 2.9f;
//				_lerpAngle = _angleToTarget;
//				_warpTimeout = 2;
//			}
//
//			if (_pos.x > 3) {
//				_pos.x = -2.9f;
//				_lerpAngle = _angleToTarget;
//				_warpTimeout = 2;
//			}
//
//			if (_pos.y <= -1.7f) {
//				_pos.y = 1.6f;
//				_lerpAngle = _angleToTarget;
//				_warpTimeout = 2;
//			}
//
//			if (_pos.y > 1.7f) {
//				_pos.y = -1.6f;
//				_lerpAngle = _angleToTarget;
//				_warpTimeout = 2;
//			}
//		} else {
//			_pos.x = Mathf.Max (-3, _pos.x);
//			_pos.x = Mathf.Min (3, _pos.x);
//			_pos.y = Mathf.Max (-1.7f, _pos.y);
//			_pos.y = Mathf.Min (1.7f, _pos.y);
//		}

		_rigidBody.MovePosition (_pos);
		gameObject.transform.eulerAngles = new Vector3(0, 0, _lerpAngle * Mathf.Rad2Deg);

		//gameObject.transform.Translate ((Vector3)_finalVelocity);
	}

	void OnDestroy () {
		var exp = (GameObject) Instantiate(_explosion, gameObject.transform.position, new Quaternion (), gameObject.transform.parent);

		foreach (var ps in exp.GetComponentsInChildren <ParticleSystem> ()) {
			var module = ps.main;
			module.startColor = new ParticleSystem.MinMaxGradient (Color.red);
		}

		Destroy (exp, 2);

		Helpers.PlayAudio (_sfxExplode, 0.5f);
	}
}
