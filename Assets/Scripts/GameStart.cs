﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStart : MonoBehaviour {
	public GameObject[] DisabledObjects;

	bool _started;

	// Use this for initialization
	void Start () {
		_started = false;

		System.Action off = () => {};
		System.Action on = () => {};

		off = () => {
			gameObject.transform.Find ("Text").GetComponent <Text> ().enabled = false;
			LeanTween.delayedCall (gameObject, 0.5f, on);
		};
		on = () => {
			gameObject.transform.Find ("Text").GetComponent <Text> ().enabled = true;
			LeanTween.delayedCall (gameObject, 1.0f, off);
		};
		off.Invoke ();

		GameObject.Find ("HighScoreText").GetComponent <Text> ().text = "High Score: " + PlayerShoot.HighScore;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space) && !_started) {
			_started = true;
			StartCoroutine( StartGame ());
		}
	}

	IEnumerator StartGame () {
		LeanTween.scale (gameObject.GetComponent <RectTransform> (), new Vector3 (10, 10, 10), 0.33f);

		yield return new WaitForSeconds (0.33f);

		foreach (var go in DisabledObjects) {
			go.SetActive (true);
		}

		gameObject.SetActive (false);

		yield return new WaitForEndOfFrame();
	}
}
