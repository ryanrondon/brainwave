﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTarget : MonoBehaviour {
	public GameObject Target;
	Vector3 _targetPos;

	Vector2 _shake;
	Vector2 _randShake;
	float _shakeDuration;
	float _shakeTime;

	// Use this for initialization
	void Start () {
		_shake.x = 0.1f;
		_shake.y = 0.1f;
		_shakeTime = 0;
		_shakeDuration = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (_shakeTime < _shakeDuration) {
			_shakeTime += Time.deltaTime;
			_randShake.x = Random.Range (-_shake.x, _shake.x);
			_randShake.y = Random.Range (-_shake.y, _shake.y);

			gameObject.transform.Translate (_randShake.x, _randShake.y, 0);
		}
	}

	public void ShakeCamera(float x, float y, float time) {
		_shake.x = x;
		_shake.y = y;
		_shakeTime = 0;
		_shakeDuration = time;
	}

	void FixedUpdate () {
		if (Target != null) {
			_targetPos = Target.transform.position;
			var delta = _targetPos - gameObject.transform.position;
			var newPos = gameObject.transform.position;
			newPos.x += delta.x * 0.05f;
			newPos.y += delta.y * 0.05f;
			newPos.z = -10;
			gameObject.transform.position = newPos;			
		}
	}
}
