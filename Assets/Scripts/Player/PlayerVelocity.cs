﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerVelocity : MonoBehaviour {
	Vector2 _finalVelocity;
	Rigidbody2D _rigidBody;
	Vector2 _pos;

	float _angleToTarget = 0;
	float _lerpAngle = 0;

	float _avoidRad = 1.0f;
	float _avoidFactor = 30;

	float _warpTimeout = 0;

	public GameObject Target;
	float _reAngleTimeout = 0;
	float _speed = 0.05f;
	float _turnRate = 2;


	// Use this for initialization
	void Start () {
		_finalVelocity = new Vector2 (0.02f, 0);
		_rigidBody = gameObject.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (_warpTimeout >= 0) {
			_warpTimeout -= Time.deltaTime;
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Destroy (gameObject);
		}
	}

	void setAngleToTarget () {
		if (Target == null) {
			return; 
		}

		if ( _reAngleTimeout <=0 ) {
			var tPos = Target.transform.position + ((Vector3) Target.GetComponent <EnemySwarmVelocity> ().FinalVelocity);
			float angle = Helpers.AngleBetweenPoints (gameObject.transform.position, tPos);

			//if (Helpers.DistanceBetweenObjects (gameObject, Target) < ) {
			//	if (Mathf.Cos(angle) < 0) {
			//		angle += Mathf.Deg2Rad * 30;
			//	}
			//	else {
			//		angle -= Mathf.Deg2Rad * 30;
			//	}
			//}
			_angleToTarget = angle;
			_reAngleTimeout = (Mathf.Rad2Deg * angle) / (_turnRate * 50);
		}
		else {
			_reAngleTimeout -= Time.fixedDeltaTime;
		}
			
		//Debug.Log ("lerped " + Mathf.Rad2Deg*_lerpAngle);
		//Debug.Log ("actual " + Mathf.Rad2Deg*_angleToTarget);

		_lerpAngle = Mathf.MoveTowardsAngle (_lerpAngle, _angleToTarget, _turnRate * Time.fixedDeltaTime);
	}

	Vector2 computeSeparationBullets () {
		Vector2 velocity = new Vector2 ();
		int neighborCount = 0;

		var allNeighbors = Physics2D.OverlapCircleAll (gameObject.transform.position, _avoidRad, 
			1 << LayerMask.NameToLayer ("Bullets") | 
			1 << LayerMask.NameToLayer ("Enemies"));
		foreach (var c in allNeighbors) {
			if (c.gameObject != gameObject && c.gameObject.tag != "Player") {
				velocity += (Vector2) Helpers.CameraRelativePos (c.gameObject) - (Vector2) Helpers.CameraRelativePos (gameObject);
				neighborCount++;
			}
		}

		if (neighborCount > 0) {
			velocity.x /= neighborCount;
			velocity.y /= neighborCount;
			velocity = new Vector2 (velocity.x - Helpers.CameraRelativePos (gameObject).x, velocity.y - Helpers.CameraRelativePos (gameObject).y);
			velocity *= -1;
			velocity.Normalize ();
			velocity /= _avoidFactor;
		}

		//Debug.Log ("Separation " + gameObject.name + " " + velocity);

		return velocity;
	}

	void searchTarget () {
		if (Target == null) {
			var allNeighbors = Physics2D.OverlapCircleAll (gameObject.transform.position, 100, 
				1 << LayerMask.NameToLayer ("Enemies"));

			foreach (var c in allNeighbors) {
				if (c.gameObject.GetComponent <Entity> () != null) {
					Target = c.gameObject;
					break;
				}
			}
		}
	}

	void FixedUpdate () {
		searchTarget ();
		setAngleToTarget ();
		gameObject.transform.eulerAngles = new Vector3(0, 0, _lerpAngle * Mathf.Rad2Deg);
		if (!gameObject.GetComponent <PlayerShoot> ().IsFiring) {
			_finalVelocity += computeSeparationBullets ();
			_speed = 0.04f;
			_turnRate = 2;
		}
		else {
			_speed = 0.02f;
			_turnRate = 6;
		}

		_finalVelocity.x = Mathf.Cos (_lerpAngle) * _speed;
		_finalVelocity.y = Mathf.Sin (_lerpAngle) * _speed;

		_pos = _rigidBody.position;
		_pos = _pos + _finalVelocity;

//		if (_warpTimeout <= 0) {
//			if (_pos.x <= -3) {
//				_pos.x = 2.9f;
//				_lerpAngle = _angleToTarget;
//				_warpTimeout = 2;
//			}
//
//			if (_pos.x > 3) {
//				_pos.x = -2.9f;
//				_lerpAngle = _angleToTarget;
//				_warpTimeout = 2;
//			}
//
//			if (_pos.y <= -1.7f) {
//				_pos.y = 1.6f;
//				_lerpAngle = _angleToTarget;
//				_warpTimeout = 2;
//			}
//
//			if (_pos.y > 1.7f) {
//				_pos.y = -1.6f;
//				_lerpAngle = _angleToTarget;
//				_warpTimeout = 2;
//			}
//		} else {
//			_pos.x = Mathf.Max (-3, _pos.x);
//			_pos.x = Mathf.Min (3, _pos.x);
//			_pos.y = Mathf.Max (-1.7f, _pos.y);
//			_pos.y = Mathf.Min (1.7f, _pos.y);
//		}

		_rigidBody.MovePosition (_pos);

		//gameObject.transform.Translate ((Vector3)_finalVelocity);
	}

	void OnDestroy () {
		if (GameObject.Find ("GameScene")) {
			GameObject.Find ("GameScene").GetComponent <EnemyManager> ().GameOver ();
		}
	}
}
