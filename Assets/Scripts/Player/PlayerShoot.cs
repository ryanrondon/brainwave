﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShoot : MonoBehaviour {
	float _fireRate = 0;
	public bool IsFiring = false;
	public Image EnergyBar;
	public Image HitScreen;
	Object _bullet;
	Object _muzzle;
	float _energy;
	public float EnergyMax;
	Object _missile;
	public float MissileCooldown = 1.0f;
	float _missileRate;
	float _missileNum = 1;

	public int KillCount = 0;
	public int MaxKills = 0;
	public int E1Kills = 0;
	public int E2Kills = 0;
	public int E3Kills = 0;
	Object _fx;
	Object _shield;

	Entity _entity;
	int _score;
	public static int HighScore;
	Text _scoreText;
	Text _powerText;
	Text _highScoreText;
	Text _healthText;

	AudioClip _sfxLaser;
	AudioClip _sfxMissile;
	AudioClip _sfxPowerup;
	AudioClip _sfxHit;

	// Use this for initialization
	void Start () {
		_bullet = Resources.Load ("PlayerBullet");
		_missile = Resources.Load ("PlayerMissile");
		_muzzle = Resources.Load ("MuzzleFlash");
		_fx = Resources.Load ("PowerUp");
		_shield = Resources.Load ("Shield");
		_energy = 9;
		EnergyMax = 9;
		_entity = gameObject.GetComponent <Entity> ();

		_scoreText = GameObject.Find ("ScoreText").GetComponent <Text> ();
		_highScoreText = GameObject.Find ("HighScoreText").GetComponent <Text> ();
		_powerText = GameObject.Find ("PowerText").GetComponent <Text> ();
		_healthText = GameObject.Find ("HealthText").GetComponent <Text> ();

		_sfxLaser = Resources.Load<AudioClip> ("Audio/laser");
		_sfxMissile = Resources.Load<AudioClip> ("Audio/missile");
		_sfxPowerup = Resources.Load<AudioClip> ("Audio/powerup");
		_sfxHit = Resources.Load<AudioClip> ("Audio/player_hit");
	}
	
	// Update is called once per frame
	void Update () {
		UpdateUI ();
		if (Input.GetKeyDown (KeyCode.Space)) {
			_fireRate = 0;
		}
		if (Input.GetKey (KeyCode.Space)) {
			if (_fireRate <= 0 && _energy >= 1) {
				shoot ();
				_fireRate = 0.15f;
				_energy -= 1;
			}
			else {
				_fireRate -= Time.deltaTime;
			}

			if (_missileRate <=0 && _energy >= 1) {
				StartCoroutine(missiles ());
				_missileRate = Mathf.Max (MissileCooldown, _fireRate * 2.0f);
			}
			else {
				_missileRate -= Time.deltaTime;
			}

			IsFiring = true;
		}

		if (Input.GetKeyUp (KeyCode.Space)) {
			IsFiring = false;
		}

		if (!IsFiring && _energy < EnergyMax) {
			_energy += Time.deltaTime * 3.0f;
			_energy = Mathf.Min (EnergyMax, _energy);
		}

		EnergyBar.GetComponent <RectTransform> ().localScale = new Vector3 (_energy/EnergyMax, 1, 1);
	}

	void UpdateUI () {
		_healthText.text = "Health: " + _entity.Life;
		_scoreText.text = "Score: " + _score;
		_highScoreText.text = "High Score: " + HighScore;
	}

	void shoot () {
		var bullet = (GameObject) Instantiate(_bullet, gameObject.transform.position, new Quaternion (), gameObject.transform.parent);

		float angle = gameObject.transform.rotation.eulerAngles.z * Mathf.Deg2Rad; //Helpers.AngleBetweenObjects (gameObject, Target);
		Vector2 bVel;
		bVel.x = Mathf.Cos (angle) * 0.09f;
		bVel.y = Mathf.Sin (angle) * 0.09f;

		bullet.GetComponent<Bullet> ().FinalVelocity = bVel;
		bullet.transform.rotation = gameObject.transform.rotation;

		var muzzle = (GameObject)Instantiate (_muzzle, new Vector3 (0.15f, 0, 0), new Quaternion (), gameObject.transform);
		muzzle.transform.localPosition = new Vector3 (-0.15f, 0, 0);
		Destroy (muzzle, 0.2f);

		Helpers.PlayAudio (_sfxLaser, 0.5f);
	}

	IEnumerator missiles () {
		for (int i = 0; i < Mathf.Floor (_missileNum/2.0f) + 1; i++) {
			var missile = (GameObject) Instantiate(_missile, gameObject.transform.position, new Quaternion (), gameObject.transform.parent);
			missile.transform.rotation = gameObject.transform.rotation;

			//missile.GetComponent <Bullet> ().HomingTarget = gameObject.GetComponent <PlayerVelocity> ().Target;

			var allNeighbors = Physics2D.OverlapCircleAll (gameObject.transform.position, 5, 1 << LayerMask.NameToLayer ("Enemies"));
			foreach (var c in allNeighbors) {
				if (c.gameObject.GetComponent<Entity> ()) {
					missile.GetComponent <Bullet> ().HomingTarget = c.gameObject;
					break;
				}
			}

			Helpers.PlayAudio (_sfxMissile, 0.3f);
			yield return new WaitForSeconds (0.1f);
		}
	}

	public void OnHit () {
		HitScreen.enabled = true;
		LeanTween.alpha (HitScreen.gameObject.GetComponent <RectTransform> (), 1.0f, 0.016f);
		LeanTween.alpha (HitScreen.gameObject.GetComponent <RectTransform> (), 0.0f, 1.0f).setOnComplete( () => {
			HitScreen.GetComponent<Image> ().enabled = false;
		}).setDelay (0.016f);
		Helpers.PlayAudio (_sfxHit, 0.66f);
	}

	public void OnKill (GameObject killed) {
		if (killed != gameObject) {
			KillCount++;

			_score = KillCount;
			if (_score > HighScore) {
				HighScore = _score;
			}

			if (KillCount % 30 == 0) {
				gameObject.GetComponent <Entity> ().Life++;
				powerupFX ();
				StartCoroutine (powerupText ("Health   Resonance   Increased!"));
			}

			if (killed.name.Contains ("Enemy1")) {
				E1Kills++;
				if (E1Kills % 3 == 0) {
					E1Kills = 0;
					EnergyMax += 1;
					powerupFX ();
					StartCoroutine (powerupText ("Energy   Resonance   Increased!"));
				}
			}
			if (killed.name.Contains ("Enemy2")) {
				E2Kills++;
				if (E2Kills % 3 == 0) {
					MissileCooldown -= 0.1f;
					E2Kills = 0;
					_missileNum++;
					powerupFX ();
					StartCoroutine (powerupText ("Missile   Resonance   Increased!"));
				}

			}
			if (killed.name.Contains ("Enemy3")) {
				E3Kills++;
				if (E3Kills % 3 == 0) {
					E3Kills = 0;
					powerupFX ();
					addShield ();
					StartCoroutine (powerupText ("Shield   Resonance   Activated!"));
				}

			}
		}
	}

	void addShield () {
		var shield = (GameObject)Instantiate (_shield);
		shield.transform.SetParent (gameObject.transform);
		shield.transform.localPosition = Vector3.zero;
		shield.transform.localScale = new Vector3 (3, 3, 3);
	}

	void powerupFX() {
		var fx = (GameObject)Instantiate (_fx);
		fx.transform.SetParent (gameObject.transform);
		fx.transform.localPosition = Vector3.zero;
		fx.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
		Destroy (fx, 2);
		Helpers.PlayAudio (_sfxPowerup, 1);
	}

	IEnumerator powerupText (string power) {
		_powerText.enabled = true;
		_powerText.text = power;
		yield return new WaitForSeconds (1.0f);
		_powerText.enabled = false;
		yield return new WaitForSeconds (0.5f);
		_powerText.enabled = true;
		yield return new WaitForSeconds (1.0f);
		_powerText.enabled = false;
		yield return new WaitForSeconds (0.5f);
		_powerText.enabled = true;
		yield return new WaitForSeconds (1.0f);
		_powerText.enabled = false;
	}
}
