﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockOnTarget : MonoBehaviour {
	public GameObject LockSprite;
	PlayerVelocity _pVel;
	Vector3 _targetPos;

	// Use this for initialization
	void Start () {
		_pVel = gameObject.GetComponent <PlayerVelocity> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {
		if (_pVel.Target != null) {
			_targetPos = _pVel.Target.transform.position + ((Vector3)_pVel.Target.GetComponent <EnemySwarmVelocity> ().FinalVelocity * 10);
			var delta = _targetPos - LockSprite.transform.position;
			var newPos = LockSprite.transform.position;
			newPos.x += delta.x * 0.2f;
			newPos.y += delta.y * 0.2f;
			newPos.z = 0;
			LockSprite.transform.position = newPos;
		} 
	}
}
